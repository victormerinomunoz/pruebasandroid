package cl.archibaldodelacruz.libreria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        /* Se crea un objeto de la clase MainActivity de la clase la librería*/
        Intent intent = new Intent(this, cl.archibaldodelacruz.lalibreria.LaLibreriaActivity.class);
        startActivity(intent);
    }
}
