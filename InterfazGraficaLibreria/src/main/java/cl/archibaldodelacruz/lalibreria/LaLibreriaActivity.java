package cl.archibaldodelacruz.lalibreria;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.archibaldodelacruz.InterfazGraficaLibreria.R;
import cl.archibaldodelacruz.calculadora.Operaciones;

public class LaLibreriaActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText numero1, numero2, calculado;
    private Button sumar, restar, multiplicar, dividir;
    private Double resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numero1 = findViewById(R.id.editText);
        numero2 = findViewById(R.id.editText2);
        calculado = findViewById(R.id.editText3);

        sumar = findViewById(R.id.button);
        restar = findViewById(R.id.button2);
        multiplicar = findViewById(R.id.button3);
        dividir = findViewById(R.id.button4);

        sumar.setOnClickListener(this);
        restar.setOnClickListener(this);
        multiplicar.setOnClickListener(this);
        dividir.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if(numero1.getText().toString().isEmpty() || numero2.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(), "Debe ingresar ambos números", Toast.LENGTH_LONG).show();

        }else{

            Operaciones nuevaOperacion = new Operaciones();
            double a= Double.parseDouble(numero1.getText().toString());
            double b= Double.parseDouble(numero2.getText().toString());
            if (view.getId() == R.id.button) {
                resultado = nuevaOperacion.sumar(a, b);
            } else if (view.getId() == R.id.button2) {
                resultado = nuevaOperacion.restar(a, b);
            } else if (view.getId() == R.id.button3) {
                resultado = nuevaOperacion.multiplicar(a, b);
            } else if (view.getId() == R.id.button4) {
                resultado = nuevaOperacion.dividir(a, b);
            }
            calculado.setText(resultado.toString());
        }

    }
}
